package com.dewmobile.account.manager;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by guoqi on 16/3/8.
 */
public class LocalPackageManager {
    private static LocalPackageManager instance;
    private List<String> pkgNameLists;
    private PackageManager pm;
    private Context mContext;
    public synchronized static LocalPackageManager getInstance(Context context){
        if (instance == null){
            instance = new LocalPackageManager(context.getApplicationContext());
        }
        return instance;
    }
    private LocalPackageManager(Context context){
        mContext = context;
        pm = context.getPackageManager();
        List<PackageInfo> pkgInfoLists = pm.getInstalledPackages(PackageManager.GET_META_DATA);
        pkgNameLists = new ArrayList<>();
        for (PackageInfo pkgInfo:pkgInfoLists){
            pkgNameLists.add(pkgInfo.packageName);
        }
        initBroadcast();
    }

    private void initBroadcast(){
        IntentFilter filter = new IntentFilter();
        filter.addAction(Intent.ACTION_PACKAGE_ADDED);
        filter.addAction(Intent.ACTION_PACKAGE_REMOVED);
        filter.addAction(Intent.ACTION_PACKAGE_REPLACED);
        filter.addDataScheme("package");
        mContext.registerReceiver(receiver, filter);
    }

    public boolean isExitPkg(String packageName){
        return pkgNameLists.contains(packageName);
    }

    public void quit(){
        if (mContext != null && receiver != null) {
            try {
                mContext.unregisterReceiver(receiver);
            }catch (Exception e){}
        }
    }

    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            Uri uri = intent.getData();
            String pkg = uri != null ? uri.getSchemeSpecificPart() : null;
            if (null == pkg) {
                return;
            }
            if (Intent.ACTION_PACKAGE_ADDED.equals(action)){
                pkgNameLists.add(pkg);
            }else if (Intent.ACTION_PACKAGE_REMOVED.equals(action)){
                pkgNameLists.remove(pkg);
            }
        }
    };
}
