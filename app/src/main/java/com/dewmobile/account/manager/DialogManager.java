package com.dewmobile.account.manager;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatTextView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Toast;

import com.dewmobile.account.R;
import com.dewmobile.account.activity.SettingsActivity;
import com.dewmobile.account.app.MyApplication;
import com.dewmobile.account.utils.EncDecUtils;
import com.dewmobile.account.utils.PreferenceConstants;
import com.dewmobile.account.utils.SpUtil;

import java.lang.reflect.Field;

/**
 * Created by guoqi on 16/4/18.
 */
public class DialogManager {
    public static final int DIALOG_TYPE_ACCESSIBILITY = 0;
    public static final int DIALOG_TYPE_MENU = 1;
    public static final int DIALOG_TYPE_INPUT_LOCAL_PASS = 1;
    private int mType;
    private Context mContext;
    private LayoutInflater mInflater;

    public DialogManager(Context context, int type) {
        mContext = context;
        mType = type;
        mInflater = LayoutInflater.from(mContext);
    }

    public interface onCheckLocalPass {
        void onMatched(boolean isMatch);
    }

    public AlertDialog getAccessibility() {
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setTitle("提示");
        builder.setMessage("需要开启自动填充功能才能使用这些帐号");
        builder.setPositiveButton("开启", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                mContext.startActivity(new Intent(mContext, SettingsActivity.class));
            }
        });
        return builder.create();
    }

    public AlertDialog getMenu(String[] items, DialogInterface.OnClickListener listener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setItems(items, listener);
        return builder.create();
    }

    public AlertDialog getInputLocalPass(final onCheckLocalPass listener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setTitle("本地密码");
        final AppCompatEditText etPass = new AppCompatEditText(mContext);
        builder.setView(etPass);
        builder.setPositiveButton("确定", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String localPass = SpUtil.getFromLocal(MyApplication.getContext(), null, PreferenceConstants.SP_LOCAL_KEY, "");
                String pass = etPass.getText().toString();
                listener.onMatched(EncDecUtils.encMD5_16(pass).equals(localPass));
            }
        });
        builder.setNegativeButton("取消", null);
        return builder.create();
    }

    public AlertDialog getSetLocalPass() {
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setTitle("本地密码");
        View view = LayoutInflater.from(mContext).inflate(R.layout.dialog_content_view_set_pass, null);
        final AppCompatEditText etPass = (AppCompatEditText) view.findViewById(R.id.et_password);
        final AppCompatEditText etConfirmPass = (AppCompatEditText) view.findViewById(R.id.et_confirm_password);
        builder.setView(view);
        builder.setPositiveButton("确定", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String pass = etPass.getText().toString();
                if (pass.equals(etConfirmPass.getText().toString())) {
                    try {
                        Field field = Dialog.class.getDeclaredField("mShowing");
                        field.setAccessible(true);
                        field.set(dialog, true);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    SpUtil.saveToLocal(MyApplication.getContext(), null, PreferenceConstants.SP_LOCAL_KEY, EncDecUtils.encMD5_16(pass));
                } else {
                    try {
                        Field field = Dialog.class.getDeclaredField("mShowing");
                        field.setAccessible(true);
                        field.set(dialog, false);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    Toast.makeText(mContext, "两次输入不一样呀！", Toast.LENGTH_SHORT).show();
                }
            }
        });
        builder.setNegativeButton("取消", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                try {
                    Field field = Dialog.class.getDeclaredField("mShowing");
                    field.setAccessible(true);
                    field.set(dialog, true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        return builder.create();
    }
}
