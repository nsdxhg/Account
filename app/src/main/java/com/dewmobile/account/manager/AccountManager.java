package com.dewmobile.account.manager;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.text.TextUtils;

import com.dewmobile.account.app.MyApplication;
import com.dewmobile.account.model.AccountModel;
import com.dewmobile.account.utils.Constants;
import com.dewmobile.account.utils.DataBaseHelper;
import com.dewmobile.account.utils.EncDecUtils;
import com.dewmobile.account.utils.PreferenceConstants;
import com.dewmobile.account.utils.SpUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by guoqi on 16/4/6.
 */
public class AccountManager {
    public static final Uri ACCOUNT_URI = Uri.parse("content://" + DataBaseHelper.AUTHORITY + "/" + DataBaseHelper.PATH_TABLE_ACCOUNT);

    private static AccountManager instance;

    private static final String QUERY_ACCOUNT = "SELECT * FROM " + DataBaseHelper.A_TABLE_NAME
            + " WHERE " + DataBaseHelper.A_COLUMN_ID + "=?";
    private static final String DELETE_ACCOUNT = "DELETE FROM " + DataBaseHelper.A_TABLE_NAME
            + " WHERE " + DataBaseHelper.A_COLUMN_ID + "=?";
    private static final String QUERY_ACCOUNT_ALL = "SELECT * FROM " + DataBaseHelper.A_TABLE_NAME;

    private List<AccountModel> accountList = new ArrayList<>();

    public static synchronized AccountManager getInstance() {
        if (instance == null) {
            instance = new AccountManager();
        }
        return instance;
    }

    public long saveAccount(AccountModel model) {
        long id = -1;
        if (model == null) {
            return -1;
        }
        DataBaseHelper dbHelper = DataBaseHelper.getInstance(MyApplication.getContext());
        synchronized (dbHelper.lock) {
            SQLiteDatabase db = dbHelper.getWritableDatabase();
            if (db.isOpen()) {
                db.beginTransaction();
                try {
                    ContentValues values = new ContentValues();
//                    String key = SpUtil.getFromLocal(MyApplication.getContext(), null, PreferenceConstants.SP_LOCAL_KEY, "");
                    String key = "";
                    String password = EncDecUtils.encBase64(EncDecUtils.encAES(model.password, TextUtils.isEmpty(key) ? Constants.ENCODE_KEY_BASE : key, 1));
                    values.put(DataBaseHelper.A_COLUMN_USERNAME, model.username);
                    values.put(DataBaseHelper.A_COLUMN_PASSWORD, password);
                    values.put(DataBaseHelper.A_COLUMN_CATEGORY, model.category);
                    values.put(DataBaseHelper.A_COLUMN_TYPE, model.type);
                    AccountModel accountModel = getAccount(model._id);
                    if (accountModel != null) {
                        db.update(DataBaseHelper.A_TABLE_NAME, values, DataBaseHelper.A_COLUMN_ID+"=?", new String[]{String.valueOf(model._id)});
                    } else {
                        id = db.insert(DataBaseHelper.A_TABLE_NAME, null, values);
                    }
                    db.setTransactionSuccessful();
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    db.endTransaction();
                }
            }
        }
        return id;
    }

    public List<AccountModel> getList() {
        if (accountList.isEmpty()) {
            accountList.addAll(getAccounts());
        }
        return accountList;
    }

    public List<AccountModel> getAccounts() {
        DataBaseHelper dbHelper = DataBaseHelper.getInstance(MyApplication.getContext());
        Cursor cursor = null;
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        if (db.isOpen()) {
            cursor = db.rawQuery(QUERY_ACCOUNT_ALL, new String[]{});
        }
        List<AccountModel> list = new ArrayList<>();
        if (cursor != null && cursor.moveToFirst()) {
            try {
                do {
                    AccountModel model = new AccountModel();
                    model._id = cursor.getInt(cursor.getColumnIndex(DataBaseHelper.A_COLUMN_ID));
                    model.username = cursor.getString(cursor.getColumnIndex(DataBaseHelper.A_COLUMN_USERNAME));
                    String password = cursor.getString(cursor.getColumnIndex(DataBaseHelper.A_COLUMN_PASSWORD));
                    model.category = cursor.getInt(cursor.getColumnIndex(DataBaseHelper.A_COLUMN_CATEGORY));
                    model.type = cursor.getInt(cursor.getColumnIndex(DataBaseHelper.A_COLUMN_TYPE));
//                    String key = SpUtil.getFromLocal(MyApplication.getContext(), null, PreferenceConstants.SP_LOCAL_KEY, "");
                    String key = "";
                    model.password = new String(EncDecUtils.decAES(EncDecUtils.decBase64(password), TextUtils.isEmpty(key) ? Constants.ENCODE_KEY_BASE.getBytes() : key.getBytes(), 1));
                    list.add(model);
                } while (cursor.moveToNext());
            } finally {
                cursor.close();
            }
        }
        return list;
    }

    public AccountModel getAccount(int id) {
        DataBaseHelper dbHelper = DataBaseHelper.getInstance(MyApplication.getContext());
        Cursor cursor = null;
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        if (db.isOpen()) {
            cursor = db.rawQuery(QUERY_ACCOUNT, new String[]{String.valueOf(id)});
        }
        AccountModel model = null;
        if (cursor != null && cursor.moveToFirst()) {
            try {
                do {
                    model = new AccountModel();
                    model._id = cursor.getInt(cursor.getColumnIndex(DataBaseHelper.A_COLUMN_ID));
                    model.username = cursor.getString(cursor.getColumnIndex(DataBaseHelper.A_COLUMN_USERNAME));
                    String password = cursor.getString(cursor.getColumnIndex(DataBaseHelper.A_COLUMN_PASSWORD));
                    model.category = cursor.getInt(cursor.getColumnIndex(DataBaseHelper.A_COLUMN_CATEGORY));
                    model.type = cursor.getInt(cursor.getColumnIndex(DataBaseHelper.A_COLUMN_TYPE));
//                    String key = SpUtil.getFromLocal(MyApplication.getContext(), null, PreferenceConstants.SP_LOCAL_KEY, "");
                    String key = "";
                    model.password = new String(EncDecUtils.decAES(EncDecUtils.decBase64(password), TextUtils.isEmpty(key) ? Constants.ENCODE_KEY_BASE.getBytes() : key.getBytes(), 1));
                } while (cursor.moveToNext());
            } finally {
                cursor.close();
            }
        }
        return model;
    }

    public int deleteAccount(int id) {
        DataBaseHelper dbHelper = DataBaseHelper.getInstance(MyApplication.getContext());
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        if (db.isOpen()) {
            return db.delete(DataBaseHelper.A_TABLE_NAME, DataBaseHelper.A_COLUMN_ID + "=?", new String[]{String.valueOf(id)});
        }
        return 0;
    }
}
