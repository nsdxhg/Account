package com.dewmobile.account.processor;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;

import com.dewmobile.account.BuildConfig;
import com.dewmobile.account.model.AccountCategory;
import com.dewmobile.account.model.AccountModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by guoqi on 16/4/11.
 */
public class LetvProcessor {

    private ClipboardManager clipboardManager;
    private static LetvProcessor instance;

    public static LetvProcessor getInstance(ClipboardManager clipboard) {
        if (instance == null) {
            instance = new LetvProcessor(clipboard);
        }
        return instance;
    }

    private LetvProcessor(ClipboardManager clipboard) {
        this.clipboardManager = clipboard;
    }

    public void loadData(AccessibilityEvent event, AccessibilityNodeInfo info, AccountModel model) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            setData(event, info, model);
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            pasteData(event, model);
        }
    }

    private void pasteData(AccessibilityEvent event, AccountModel model) {
        if (event.getEventType() == AccessibilityEvent.TYPE_WINDOW_STATE_CHANGED) {
            String className = event.getClassName().toString();
            if (className.equals("com.letv.android.client.activity.LetvLoginActivity")) {
                AccessibilityNodeInfo info = event.getSource();
                List<AccessibilityNodeInfo> nodes = info.findAccessibilityNodeInfosByViewId("com.letv.android.client:id/letv_account");
                for (AccessibilityNodeInfo node : nodes) {
                    if (node.getClassName().equals("android.widget.EditText") && node.isEnabled()) {
                        clipboardManager.setPrimaryClip(ClipData.newPlainText("username", model.username));
                        node.performAction(AccessibilityNodeInfo.ACTION_FOCUS);
                        node.performAction(AccessibilityNodeInfo.ACTION_PASTE);
                        clipboardManager.setPrimaryClip(ClipData.newPlainText("username", ""));
                    }
                }
                List<AccessibilityNodeInfo> nodeInfo = info.findAccessibilityNodeInfosByViewId("com.letv.android.client:id/letv_account_password");
                for (AccessibilityNodeInfo node : nodeInfo) {
                    if (node.getClassName().equals("android.widget.EditText") && node.isEnabled()) {
                        clipboardManager.setPrimaryClip(ClipData.newPlainText("password", model.password));
                        node.performAction(AccessibilityNodeInfo.ACTION_FOCUS);
                        node.performAction(AccessibilityNodeInfo.ACTION_PASTE);
                        clipboardManager.setPrimaryClip(ClipData.newPlainText("password", ""));
                    }
                }
            }
        }
    }

    private void setData(AccessibilityEvent event, AccessibilityNodeInfo info, AccountModel model) {
        if (event.getEventType() == AccessibilityEvent.TYPE_WINDOW_STATE_CHANGED) {
            String className = event.getClassName().toString();
            if (className.equals("com.letv.android.client.activity.LetvLoginActivity")) {
                List<AccessibilityNodeInfo> nodes = info.findAccessibilityNodeInfosByViewId("com.letv.android.client:id/letv_account");
                for (AccessibilityNodeInfo node : nodes) {
                    if (node.getClassName().equals("android.widget.EditText") && node.isEnabled()) {
                        Bundle arguments = new Bundle();
                        arguments.putCharSequence(AccessibilityNodeInfo.ACTION_ARGUMENT_SET_TEXT_CHARSEQUENCE,
                                model.username);
                        node.performAction(AccessibilityNodeInfo.ACTION_SET_TEXT, arguments);
                    }
                }
                List<AccessibilityNodeInfo> nodeInfo = info.findAccessibilityNodeInfosByViewId("com.letv.android.client:id/letv_account_password");
                for (AccessibilityNodeInfo node : nodeInfo) {
                    if (node.getClassName().equals("android.widget.EditText") && node.isEnabled()) {
                        Bundle arguments = new Bundle();
                        arguments.putCharSequence(AccessibilityNodeInfo.ACTION_ARGUMENT_SET_TEXT_CHARSEQUENCE,
                                model.password);
                        node.performAction(AccessibilityNodeInfo.ACTION_SET_TEXT, arguments);
                    }
                }
            }
        }
    }
}
