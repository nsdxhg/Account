package com.dewmobile.account.processor;

import android.content.ClipboardManager;
import android.content.Context;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;

import com.dewmobile.account.app.MyApplication;
import com.dewmobile.account.model.AccountModel;
import com.dewmobile.account.utils.Constants;

/**
 * Created by guoqi on 16/6/3.
 */
public class LoginManager {
    private static LoginManager instance;
    private ClipboardManager clipboardManager = null;

    private LoginManager(){
        clipboardManager = (ClipboardManager) MyApplication.getContext().getSystemService(Context.CLIPBOARD_SERVICE);
    }

    public synchronized static LoginManager getInstance(){
        if (instance == null){
            instance = new LoginManager();
        }
        return instance;
    }

    public void startAutoInput(AccessibilityEvent event, AccessibilityNodeInfo nodeInfo, AccountModel dataModel){
        if (Constants.LETV_PACKAGE_NAME.equals(event.getPackageName())) {
            LetvProcessor letvProcessor = LetvProcessor.getInstance(clipboardManager);
            letvProcessor.loadData(event,nodeInfo,dataModel);
        } else if (Constants.IQIYI_PACKAGE_NAME.equals(event.getPackageName())) {
            QiyiProcessor qiyiProcessor = QiyiProcessor.getInstance(clipboardManager);
            qiyiProcessor.loadData(event,nodeInfo,dataModel);
        }else if (Constants.YOUKU_PACKAGE_NAME.equals(event.getPackageName())) {
            YoukuProcessor youkuProcessor = YoukuProcessor.getInstance(clipboardManager);
            youkuProcessor.loadData(event,nodeInfo,dataModel);
        }
    }
}
