package com.dewmobile.account.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.HashMap;

/**
 * Created by guoqi on 16/4/6.
 */
public class AccountModel implements Serializable {
    private static final long serialVersionUID = 1542480798319090338L;

    public int _id;
    @SerializedName("accounttype")
    public int category;
    @SerializedName("name")
    public String username;
    @SerializedName("password")
    public String password;
    public int type;
}
