package com.dewmobile.account.model;

import android.content.Context;
import android.content.res.AssetManager;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;
import com.google.gson.reflect.TypeToken;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by guoqi on 16/4/11.
 */
public class AccountCategory implements Serializable{
    public static final String localData= "category.json";
    private static List<AccountCategory> list;
    @SerializedName("id")
    public int _id;
    @SerializedName("name")
    public String name;
    @SerializedName("logourl")
    public String icon;
    @SerializedName("pkgname")
    public String pkg;

    @Override
    public String toString() {
        return this.name;
    }

    public static List<AccountCategory> getLocalList(Context context){
        if (list == null){
            Gson gson = new Gson();
            String jsonStr = getJson(context, localData);
            list = gson.fromJson(jsonStr,new TypeToken<List<AccountCategory>>(){}.getType());
        }
        return list;
    }

    private static String getJson(Context context, String fileName) {

        StringBuilder stringBuilder = new StringBuilder();
        try {
            AssetManager assetManager = context.getAssets();
            BufferedReader bf = new BufferedReader(new InputStreamReader(
                    assetManager.open(fileName)));
            String line;
            while ((line = bf.readLine()) != null) {
                stringBuilder.append(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return stringBuilder.toString();
    }

}
