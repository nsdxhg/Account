package com.dewmobile.account.provider;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.support.annotation.Nullable;

import com.dewmobile.account.utils.DataBaseHelper;

/**
 * Created by guoqi on 16/4/6.
 */
public class AccountProvider extends ContentProvider {
    private UriMatcher sURIMatcher = new UriMatcher(UriMatcher.NO_MATCH);
    private static final int ACCOUNT = 1;
    private DataBaseHelper dbHelper = null;

    @Override
    public boolean onCreate() {
        sURIMatcher.addURI(DataBaseHelper.AUTHORITY,DataBaseHelper.PATH_TABLE_ACCOUNT,ACCOUNT);
        dbHelper = DataBaseHelper.getInstance(getContext());
        return true;
    }

    @Nullable
    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        int match = sURIMatcher.match(uri);
        if(match == ACCOUNT){
            SQLiteDatabase db = dbHelper.getWritableDatabase();
            return db.query(DataBaseHelper.A_TABLE_NAME,projection,selection,selectionArgs,null,null,sortOrder);
        }
        return null;
    }

    @Nullable
    @Override
    public String getType(Uri uri) {
        return null;
    }

    @Nullable
    @Override
    public Uri insert(Uri uri, ContentValues values) {
        return null;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        return 0;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        return 0;
    }
}
