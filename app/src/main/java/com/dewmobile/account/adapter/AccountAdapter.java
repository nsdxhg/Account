package com.dewmobile.account.adapter;

import android.content.Context;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.dewmobile.account.R;
import com.dewmobile.account.model.AccountCategory;
import com.dewmobile.account.model.AccountModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by guoqi on 16/4/6.
 */
public class AccountAdapter extends RecyclerView.Adapter<AccountAdapter.AccountViewHolder> {
    public static final int ACTION_ITEM_CLICK = 0;
    public static final int ACTION_ITEM_EDIT = 1;
    public static final int ACTION_ITEM_DELETE = 2;
    private Context mContext;
    private List<AccountModel> mDataList;
    private List<AccountCategory> mCategoryList;
    private OnItemClickListener<AccountModel> mListener;

    public AccountAdapter(Context context, OnItemClickListener<AccountModel> listener) {
        super();
        mContext = context;
        mDataList = new ArrayList<>();
        mListener = listener;
        mCategoryList = AccountCategory.getLocalList(mContext);
    }

    @Override
    public AccountViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new AccountViewHolder(LayoutInflater.from(mContext).inflate(R.layout.item_list_account, parent, false));
    }

    @Override
    public void onBindViewHolder(AccountViewHolder holder, int position) {
        AccountModel model = mDataList.get(position);
        holder.updateData(model, position);
    }

    @Override
    public int getItemCount() {
        return mDataList.size();
    }

    public void setData(List<AccountModel> list) {
        mDataList.clear();
        if (list != null && !list.isEmpty()) {
            mDataList.addAll(list);
        }
        notifyDataSetChanged();
    }

    public class AccountViewHolder extends RecyclerView.ViewHolder {
        private AppCompatImageView ivIcon;
        private AppCompatTextView tvUsername, tvLabel, tvEdit, tvDelete;
        private AppCompatCheckBox cbLock;

        public AccountViewHolder(View itemView) {
            super(itemView);
            ivIcon = (AppCompatImageView) itemView.findViewById(R.id.iv_icon);
            tvUsername = (AppCompatTextView) itemView.findViewById(R.id.tv_username);
            tvLabel = (AppCompatTextView) itemView.findViewById(R.id.tv_label);
            tvEdit = (AppCompatTextView) itemView.findViewById(R.id.tv_action_edit);
            tvDelete = (AppCompatTextView) itemView.findViewById(R.id.tv_action_delete);
            cbLock = (AppCompatCheckBox) itemView.findViewById(R.id.cb_lock);
        }

        public void updateData(final AccountModel model, final int position) {
            tvUsername.setText(model.username);
            AccountCategory category = findCategoryById(model.category);
            if (category != null && !TextUtils.isEmpty(category.icon)) {
                Glide.with(mContext).load(category.icon).into(ivIcon);
                tvLabel.setText(category.name);
            }
            cbLock.setChecked(model.type != 0);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mListener.onItemClick(ACTION_ITEM_CLICK, model, position);
                }
            });
            tvEdit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mListener.onItemClick(ACTION_ITEM_EDIT, model, position);
                }
            });
            tvDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mListener.onItemClick(ACTION_ITEM_DELETE, model, position);
                }
            });
            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
//                    mListener.onItemLongClick(ACTION_ITEM_CLICK,model,position);
                    return false;
                }
            });
        }
    }

    private AccountCategory findCategoryById(int id) {
        for (AccountCategory category : mCategoryList) {
            if (category._id == id) {
                return category;
            }
        }
        return null;
    }
}
