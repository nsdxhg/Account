package com.dewmobile.account.adapter;

import android.content.Context;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.dewmobile.account.R;
import com.dewmobile.account.model.AccountCategory;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by guoqi on 16/4/6.
 */
public class AccountCategoryAdapter extends RecyclerView.Adapter<AccountCategoryAdapter.AccountViewHolder>{
    public static final int ACTION_ITEM_CLICK = 0;
    private Context mContext;
    private List<AccountCategory> mDataList;
    private OnItemClickListener<AccountCategory> mListener;

    public AccountCategoryAdapter(Context context, OnItemClickListener<AccountCategory> listener) {
        super();
        mContext = context;
        mDataList = new ArrayList<>();
        mListener = listener;
    }

    @Override
    public AccountViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new AccountViewHolder(LayoutInflater.from(mContext).inflate(R.layout.item_grid_account,parent,false));
    }

    @Override
    public void onBindViewHolder(AccountViewHolder holder, int position) {
        AccountCategory model = mDataList.get(position);
        holder.updateData(model,position);
    }

    @Override
    public int getItemCount() {
        return mDataList.size();
    }

    public void setData(List<AccountCategory> list){
        mDataList.clear();
        if (list != null && !list.isEmpty()){
            mDataList.addAll(list);
        }
        notifyDataSetChanged();
    }

    public class AccountViewHolder extends RecyclerView.ViewHolder{
        private AppCompatImageView ivIcon;
        private AppCompatTextView tvUsername;
        public AccountViewHolder(View itemView) {
            super(itemView);
            ivIcon = (AppCompatImageView) itemView.findViewById(R.id.iv_icon);
            tvUsername = (AppCompatTextView) itemView.findViewById(R.id.tv_username);
        }

        public void updateData(final AccountCategory model, final int position){
            if (!TextUtils.isEmpty(model.icon)) {
                Glide.with(mContext).load(model.icon).into(ivIcon);
            }
            tvUsername.setText(model.name);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mListener.onItemClick(ACTION_ITEM_CLICK,model,position);
                }
            });
            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    mListener.onItemLongClick(ACTION_ITEM_CLICK,model,position);
                    return false;
                }
            });
        }
    }
}
