package com.dewmobile.account.adapter;

/**
 * Created by guoqi on 16/4/12.
 */
public interface OnItemClickListener<T> {
    void onItemClick(int action, T dataModel, int dataPosition);

    void onItemLongClick(int action, T dataModel, int dataPosition);
}
