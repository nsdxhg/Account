package com.dewmobile.account.app;

import android.app.Application;
import android.content.Context;

import com.umeng.message.PushAgent;

/**
 * Created by guoqi on 16/4/6.
 */
public class MyApplication extends Application {
    private static Context context;
    @Override
    public void onCreate() {
        super.onCreate();
        context = getApplicationContext();

        PushAgent mPushAgent = PushAgent.getInstance(context);
        mPushAgent.enable();
    }

    public static Context getContext(){
        return context;
    }
}
