package com.dewmobile.account.utils;

import android.accessibilityservice.AccessibilityServiceInfo;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.os.Build;
import android.provider.Settings;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityManager;

import com.dewmobile.account.app.MyApplication;
import com.dewmobile.account.manager.DialogManager;

import java.util.List;
import java.util.Locale;

/**
 * Created by guoqi on 16/4/14.
 */
public class AccessibilityUtil {
    public static final String ACTION_SEND_DATA= "data.action.accessibility.com";

    /**
     * check if the device support auto install by accessbility service
     * @return
     */
    public static boolean isAccessibilitySupported() {
        String language = Locale.getDefault().getLanguage();
        Intent intent = new Intent(Settings.ACTION_ACCESSIBILITY_SETTINGS);
        List<ResolveInfo> infos = MyApplication.getContext().getPackageManager().queryIntentActivities(intent, 0);
        return !((!"zh".equals(language) && !"en".equals(language)) || Build.VERSION.SDK_INT < 16
                || infos == null || infos.isEmpty());
    }

    /**
     * check if our accessiblity service is enabled
     * @param context
     * @return
     */
    public static boolean isAccessibilityEnabled(Context context) {

        AccessibilityManager am = (AccessibilityManager) context
                .getSystemService(Context.ACCESSIBILITY_SERVICE);

        List<AccessibilityServiceInfo> runningServices = am
                .getEnabledAccessibilityServiceList(AccessibilityEvent.TYPES_ALL_MASK);
        for (AccessibilityServiceInfo service : runningServices) {
            if(service.getId().startsWith(context.getPackageName()+"/") && service.getId().endsWith(".service.AutoLoginService") ){
                return true;
            }
        }
        return false;
    }

    public static boolean checkAndStart(Context context){
        boolean enable = isAccessibilityEnabled(context);
        if (!enable){
            DialogManager dialogManager = new DialogManager(context,DialogManager.DIALOG_TYPE_ACCESSIBILITY);
            dialogManager.getAccessibility().show();
//            context.startActivity(new Intent(Settings.ACTION_ACCESSIBILITY_SETTINGS));
        }
        return enable;
    }
}
