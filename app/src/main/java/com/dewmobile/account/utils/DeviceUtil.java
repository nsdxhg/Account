package com.dewmobile.account.utils;

import android.content.Context;
import android.net.wifi.WifiInfo;
import android.os.Build;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.text.TextUtils;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.util.Random;

/**
 * Created by guoqi on 16/4/18.
 */
public class DeviceUtil {
    private static String imei;
    private static String imsi;
    private static String androidId;
    private static String mac;

    public static String getIMEI(Context context) {
        if (imei != null) {
            return imei;
        } else {
            String imeiCache = SpUtil.getFromLocal(context, null, PreferenceConstants.SP_IMEI, "");
            if (!TextUtils.isEmpty(imeiCache)) {
                imei = imeiCache;
                return imei;
            } else {
                String im = null;
                TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);

                try {
                    im = tm.getDeviceId();
                } catch (Exception var5) {
                    ;
                }

                if (im != null) {
                    im = im.toUpperCase();
                }

                if (TextUtils.isEmpty(im)) {
                    im = "KY" + Build.MODEL;
                    if (Build.VERSION.SDK_INT > 8) {
                        im = im + Build.SERIAL;
                    }

                    im = im + Long.toHexString((new Random(System.currentTimeMillis())).nextLong());
                    im.replace(" ", "z");
                }

                imei = im;
                SpUtil.saveToLocal(context, null, PreferenceConstants.SP_IMEI, imei);
                return imei;
            }
        }
    }

    public static String getIMSI(Context context) {
        if (imsi != null) {
            return imsi;
        } else {
            String imsiCache = SpUtil.getFromLocal(context, null, PreferenceConstants.SP_IMSI, "");
            if (!TextUtils.isEmpty(imsiCache)) {
                imsi = imsiCache;
                return imsi;
            } else {
                String im = null;
                TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);

                try {
                    im = tm.getSubscriberId();
                } catch (Exception var5) {
                    ;
                }

                if (im != null) {
                    im = im.toUpperCase();
                }

                if (TextUtils.isEmpty(im)) {
                    im = "KY" + Build.MODEL;
                    if (Build.VERSION.SDK_INT > 8) {
                        im = im + Build.SERIAL;
                    }

                    im = im + Long.toHexString((new Random(System.currentTimeMillis())).nextLong());
                    im.replace(" ", "z");
                }

                imsi = im;
                SpUtil.saveToLocal(context, null, PreferenceConstants.SP_IMSI, imsi);
                return imsi;
            }
        }
    }

    public static String getAndroidId(Context context) {
        if (androidId != null) {
            return androidId;
        } else {
            String androidIdCathe = SpUtil.getFromLocal(context, null, PreferenceConstants.SP_ANDROID_ID, "");
            if (!TextUtils.isEmpty(androidIdCathe)) {
                androidId = androidIdCathe;
                return androidId;
            } else {
                String android = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
                androidId = android;
                SpUtil.saveToLocal(context, null, PreferenceConstants.SP_ANDROID_ID, androidId);
                return androidId;
            }
        }
    }

    public static String getMac() {
        if (!TextUtils.isEmpty(mac)) {
            return mac;
        } else {
            String macSerial = null;
            String str = "";

            try {
                Process pp = Runtime.getRuntime().exec("cat /sys/class/net/wlan0/address ");
                InputStreamReader ir = new InputStreamReader(pp.getInputStream());
                LineNumberReader input = new LineNumberReader(ir);

                for (; null != str; ) {
                    str = input.readLine();
                    if (str != null) {
                        macSerial = str.trim();// 去空格
                        break;
                    }
                }
            } catch (IOException ex) {
                // 赋予默认值
                ex.printStackTrace();
            }
            mac = macSerial;
            return macSerial;
        }
    }
}
