package com.dewmobile.account.utils;

import android.content.Context;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

/**
 * Created by guoqi on 16/4/13.
 */
public class VolleyUtils {
    private static RequestQueue DEFAULT_QUEUE;

    public static synchronized RequestQueue getDefaultRequestQueue(Context context) {
        if(DEFAULT_QUEUE == null) {
            DEFAULT_QUEUE = Volley.newRequestQueue(context,new OkHttpStack());
        }
        return DEFAULT_QUEUE;
    }
}
