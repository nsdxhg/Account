package com.dewmobile.account.utils;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.text.TextUtils;

import java.util.List;

/**
 * Created by guoqi on 16/4/14.
 */
public class PackageUtil {
    public static boolean checkApkExist(Context context, Intent intent) {
        List<ResolveInfo> list =  context.getPackageManager().queryIntentActivities(intent, 0);
        if(list.size() > 0){
            return true;
        }
        return false;
    }

    public static boolean isApkInstalled(Context context, String packageName){
        try {
            PackageInfo packageInfo = context.getPackageManager().getPackageInfo(packageName, 0);
            if(packageInfo != null){
                return true;
            }
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
        return false;
    }
}
