package com.dewmobile.account.utils;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.dewmobile.account.BuildConfig;

import java.util.Objects;

/**
 * Created by guoqi on 16/4/6.
 */
public class DataBaseHelper extends SQLiteOpenHelper{
    public static final String AUTHORITY = BuildConfig.APPLICATION_ID + ".account";
    public static final String PATH_TABLE_ACCOUNT = "account";
    private static int DB_VERSION = 1;
    private static DataBaseHelper instance;

    public static final String A_TABLE_NAME = "account";
    public static final String A_COLUMN_ID = "a_id";
    public static final String A_COLUMN_USERNAME = "a_u";
    public static final String A_COLUMN_PASSWORD = "a_p";
    public static final String A_COLUMN_CATEGORY = "a_c";
    public static final String A_COLUMN_TYPE = "a_t";

    public final Object lock = new Object();

    private static final String ACCOUNT_TABLE_CREATE = "CREATE TABLE "
            + A_TABLE_NAME + " ("
            + A_COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + A_COLUMN_USERNAME + " TEXT, "
            + A_COLUMN_PASSWORD + " TEXT, "
            + A_COLUMN_CATEGORY + " INTEGER, "
            + A_COLUMN_TYPE + " INTEGER)";
    private DataBaseHelper(Context context) {
        super(context, "account.db", null, DB_VERSION);
    }

    public static synchronized DataBaseHelper getInstance(Context context){
        if (instance == null){
            instance = new DataBaseHelper(context);
        }
        return instance;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(ACCOUNT_TABLE_CREATE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        for (int version = (oldVersion + 1); version <= newVersion; version++) {
            updateDB(db, version);
        }
    }

    private void updateDB(SQLiteDatabase db, int version) {
        switch (version) {

        }
    }
}
