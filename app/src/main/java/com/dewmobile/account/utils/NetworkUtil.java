package com.dewmobile.account.utils;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.text.TextUtils;

import java.util.HashMap;
import java.util.Locale;

/**
 * Created by guoqi on 16/4/18.
 */
public class NetworkUtil {
    private static HashMap<String, String> headers;
    private static int versionCode = -1;

    public synchronized static HashMap<String, String> getHeaders(Context context) {
        if (headers == null) {
            HashMap<String, String> header = new HashMap<String, String>();
            String imei = DeviceUtil.getIMEI(context);
            if (TextUtils.isEmpty(imei)) {
                imei = "unknown";
            }
            header.put("X-IMEI", imei);
            String mac = DeviceUtil.getMac();
            if (TextUtils.isEmpty(mac)) {
                mac = "unknown";
            }
            header.put("X-MAC", mac);
            String imsi = DeviceUtil.getIMSI(context);
            if (TextUtils.isEmpty(imsi) || imsi.length() > 15) {
                imsi = "unknown";
            }
            header.put("X-IMSI", imsi);
            String androidId = DeviceUtil.getAndroidId(context);
            if (TextUtils.isEmpty(androidId)) {
                androidId = "unknown";
            }
            header.put("X-ANDROIDID", androidId);
            header.put("X-VC", "" + getVersionCode(context));
            header.put("X-CHN", getChannel(context));
            header.put("X-Date", String.valueOf(System.currentTimeMillis()));
            header.put("Cache-Control", "no-cache");

            headers = header;
        }
        return new HashMap<String, String>(headers);
    }

    public static String getChannel(Context context) {
        String channel = null;
        String pkg = context.getPackageName();
        ApplicationInfo info;
        try {
            info = context.getPackageManager().getApplicationInfo(pkg,
                    PackageManager.GET_META_DATA);
            channel = info.metaData.getString("UMENG_CHANNEL");
        } catch (Exception e) {
        }
        return channel;
    }

    public static int getVersionCode(Context context) {
        if (versionCode != -1) {
            return versionCode;
        }
        try {
            PackageInfo packInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            versionCode = packInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
        }
        return versionCode;
    }
}
