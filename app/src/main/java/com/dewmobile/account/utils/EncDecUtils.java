package com.dewmobile.account.utils;

import java.io.ByteArrayOutputStream;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;

/**
 * A utility class for common encryption algorithms
 */
public class EncDecUtils {
	private static final char[] BASE64_ENCODECHARS = new char[] { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H',
			'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'a',
			'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't',
			'u', 'v', 'w', 'x', 'y', 'z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '+', '/' };
	private static final byte[] BASE64_DECODECHARS = new byte[] { -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
			-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
			-1, -1, -1, -1, -1, -1, -1, -1, 62, -1, -1, -1, 63, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, -1,
			-1, -1, -1, -1, -1, -1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
			21, 22, 23, 24, 25, -1, -1, -1, -1, -1, -1, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38,
			39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, -1, -1, -1, -1, -1 };

	/**
	 * Base64-encode the given data and return a newly allocated String with the
	 * result.
	 * 
	 * @param data
	 *            the data to encode
	 */
	public static String encBase64(byte[] data) {
		StringBuffer sb = new StringBuffer();
		int len = data.length;
		int i = 0;
		int b1, b2, b3;
		while (i < len) {
			b1 = data[i++] & 0xff;
			if (i == len) {
				sb.append(BASE64_ENCODECHARS[b1 >>> 2]);
				sb.append(BASE64_ENCODECHARS[(b1 & 0x3) << 4]);
				sb.append("==");
				break;
			}
			b2 = data[i++] & 0xff;
			if (i == len) {
				sb.append(BASE64_ENCODECHARS[b1 >>> 2]);
				sb.append(BASE64_ENCODECHARS[((b1 & 0x03) << 4) | ((b2 & 0xf0) >>> 4)]);
				sb.append(BASE64_ENCODECHARS[(b2 & 0x0f) << 2]);
				sb.append("=");
				break;
			}
			b3 = data[i++] & 0xff;
			sb.append(BASE64_ENCODECHARS[b1 >>> 2]);
			sb.append(BASE64_ENCODECHARS[((b1 & 0x03) << 4) | ((b2 & 0xf0) >>> 4)]);
			sb.append(BASE64_ENCODECHARS[((b2 & 0x0f) << 2) | ((b3 & 0xc0) >>> 6)]);
			sb.append(BASE64_ENCODECHARS[b3 & 0x3f]);
		}
		return sb.toString();
	}

	/**
	 * Base64-encode the given data and return a newly allocated String with the
	 * result.
	 * 
	 * @param data
	 *            the input String to encode
	 */
	public static String encBase64(String data) {
		return encBase64(data.getBytes());
	}

	/**
	 * Decode the Base64-encoded data in input and return the data in a new byte
	 * array.
	 * 
	 * @param data
	 *            the input byte to decode, which is converted to bytes using
	 *            the default charset
	 */
	public static byte[] decBase64(byte[] data) {
		int len = data.length;
		ByteArrayOutputStream buf = new ByteArrayOutputStream(len);
		int i = 0;
		int b1, b2, b3, b4;

		while (i < len) {

			/* b1 */
			do {
				b1 = BASE64_DECODECHARS[data[i++]];
			} while (i < len && b1 == -1);
			if (b1 == -1) {
				break;
			}

			/* b2 */
			do {
				b2 = BASE64_DECODECHARS[data[i++]];
			} while (i < len && b2 == -1);
			if (b2 == -1) {
				break;
			}
			buf.write((int) ((b1 << 2) | ((b2 & 0x30) >>> 4)));

			/* b3 */
			do {
				b3 = data[i++];
				if (b3 == 61) {
					return buf.toByteArray();
				}
				b3 = BASE64_DECODECHARS[b3];
			} while (i < len && b3 == -1);
			if (b3 == -1) {
				break;
			}
			buf.write((int) (((b2 & 0x0f) << 4) | ((b3 & 0x3c) >>> 2)));

			/* b4 */
			do {
				b4 = data[i++];
				if (b4 == 61) {
					return buf.toByteArray();
				}
				b4 = BASE64_DECODECHARS[b4];
			} while (i < len && b4 == -1);
			if (b4 == -1) {
				break;
			}
			buf.write((int) (((b3 & 0x03) << 6) | b4));
		}
		return buf.toByteArray();
	}

	/**
	 * Decode the Base64-encoded data in input and return the data in a new byte
	 * array.
	 * 
	 * @param data
	 *            the input String to decode, which is converted to bytes using
	 *            the default charset
	 */
	public static byte[] decBase64(String data) {
		return decBase64(data.getBytes());
	}

	/**
	 * RC4 algorithm
	 * 
	 * @param b_key
	 *            secret key
	 * @param buf
	 *            the byte to enc or dec
	 * @return the result of enc or dec
	 */
	public static byte[] rc4Method(byte[] b_key, byte[] buf) {
		byte state[] = new byte[256];

		for (int i = 0; i < 256; i++) {
			state[i] = (byte) i;
		}
		byte tmp;
		int x = 0;
		int y = 0;
		int index1 = 0;
		int index2 = 0;
		if (b_key == null || b_key.length == 0) {
			return null;
		}
		for (int i = 0; i < 256; i++) {
			index2 = ((b_key[index1] & 0xff) + (state[i] & 0xff) + index2) & 0xff;
			tmp = state[i];
			state[i] = state[index2];
			state[index2] = tmp;
			index1 = (index1 + 1) % b_key.length;
		}

		int xorIndex;

		if (buf == null) {
			return null;
		}

		byte[] result = new byte[buf.length];

		for (int i = 0; i < buf.length; i++) {
			x = (x + 1) & 0xff;
			y = ((state[x] & 0xff) + y) & 0xff;
			tmp = state[x];
			state[x] = state[y];
			state[y] = tmp;
			xorIndex = ((state[x] & 0xff) + (state[y] & 0xff)) & 0xff;
			result[i] = (byte) (buf[i] ^ state[xorIndex]);
		}
		return result;
	}

	/**
	 * RC4 algorithm
	 * 
	 * @param key
	 *            secret key
	 * @param buf
	 *            the input String to enc or dec
	 * @return the result of enc or dec
	 */
	public static byte[] rc4Method(String key, String buf) {
		return rc4Method(key.getBytes(), buf.getBytes());
	}

	/**
	 * MD5 algorithm
	 * 
	 * @param data
	 *            the byte array to decode
	 * @return the computed md5 hash value
	 */
	public static byte[] encMD5(byte[] data) {
		byte[] keymd5 = null;
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			keymd5 = md.digest(data);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return keymd5;
	}

	/**
	 * MD5 algorithm
	 * 
	 * @param data
	 *            the String to decode
	 * @return the computed md5 hash value
	 */
	public static byte[] encMD5(String data) {
		return encMD5(data.getBytes());
	}

	/**
	 * MD5 algorithm
	 * 
	 * @param plainText
	 *            the String to md5
	 * @return the computed md5 HexString in 32bytes
	 */
	public static String encMD5_32(String plainText) {
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			md.update(plainText.getBytes());
			byte b[] = md.digest();
			int i;
			StringBuffer buf = new StringBuffer("");
			for (int offset = 0; offset < b.length; offset++) {
				i = b[offset];
				if (i < 0)
					i += 256;
				if (i < 16)
					buf.append("0");
				buf.append(Integer.toHexString(i));
			}
			return buf.toString();
		} catch (NoSuchAlgorithmException e) {
			return null;
		}
	}

	/**
	 * MD5 algorithm
	 * 
	 * @param plainText
	 *            the String to md5
	 * @return the computed md5 HexString in 16bytes
	 */
	public static String encMD5_16(String plainText) {
		return encMD5_32(plainText).substring(8, 24);
	}
	
	public static String encMD5_16(String plainText,int startPos,int endPos) {
		return encMD5_32(plainText).substring(startPos, endPos);
	}

	/**
	 * AES/ECB encryption algorithm
	 * 
	 * @param content
	 *            the content byte to encode
	 * @param encKey
	 *            secret key
	 * @param pad
	 *            0,NoPadding;1,Padding
	 * @return the final bytes from the transformation.
	 */
	public static byte[] encAES(byte[] content, byte[] encKey, int pad) {
		try {
			SecretKeySpec key = new SecretKeySpec(encKey, "AES");
			Cipher cipher = null;
			if (pad == 0) {
				cipher = Cipher.getInstance("AES/ECB/NoPadding");
			} else {
				cipher = Cipher.getInstance("AES");
			}
			cipher.init(Cipher.ENCRYPT_MODE, key);
			byte[] result = cipher.doFinal(content);
			return result;
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (NoSuchPaddingException e) {
			e.printStackTrace();
		} catch (InvalidKeyException e) {
			e.printStackTrace();
		} catch (IllegalBlockSizeException e) {
			e.printStackTrace();
		} catch (BadPaddingException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * AES/ECB encryption algorithm
	 * 
	 * @param content
	 *            the input String to encode, which is converted to bytes using
	 *            the default charset
	 * @param encKey
	 *            secret key
	 * @param pad
	 *            0,NoPadding;1,Padding
	 * @return the final bytes from the transformation.
	 */
	public static byte[] encAES(String content, String encKey, int pad) {
		return encAES(content.getBytes(), encKey.getBytes(), pad);
	}

	/**
	 * AES/ECB decryption algorithm
	 * 
	 * @param content
	 *            the content byte to decode
	 * @param decKey
	 *            secret key
	 * @param pad
	 *            0,NoPadding;1,Padding
	 * @return the final bytes from the transformation.
	 */
	public static byte[] decAES(byte[] content, byte[] decKey, int pad) {
		try {
			SecretKeySpec key = new SecretKeySpec(decKey, "AES");
			Cipher cipher = null;
			if (pad == 0) {
				cipher = Cipher.getInstance("AES/ECB/NoPadding");
			} else {
				cipher = Cipher.getInstance("AES");
			}
			cipher.init(Cipher.DECRYPT_MODE, key);
			byte[] result = cipher.doFinal(content);
			return result;
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (NoSuchPaddingException e) {
			e.printStackTrace();
		} catch (InvalidKeyException e) {
			e.printStackTrace();
		} catch (IllegalBlockSizeException e) {
			e.printStackTrace();
		} catch (BadPaddingException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * AES/ECB decryption algorithm
	 * 
	 * @param content
	 *            the input String to decode, which is converted to bytes using
	 *            the default charset
	 * @param decKey
	 *            secret key
	 * @param pad
	 *            0,NoPadding;1,Padding
	 * @return the final bytes from the transformation.
	 */
	public static byte[] decAES(String content, String decKey, int pad) {
		return decAES(content.getBytes(), decKey.getBytes(), pad);
	}


	/**
	 * 将字节数组转换成16进制字符串
	 *
	 * @param buf
	 * @return
	 */
	private static String asHex(byte buf[]) {
		StringBuffer strbuf = new StringBuffer(buf.length * 2);
		int i;
		for (i = 0; i < buf.length; i++) {
			if (((int) buf[i] & 0xff) < 0x10)//小于十前面补零
				strbuf.append("0");
			strbuf.append(Long.toString((int) buf[i] & 0xff, 16));
		}
		return strbuf.toString();
	}

	/**
	 * 将16进制字符串转换成字节数组
	 *
	 * @param src
	 * @return
	 */
	private static byte[] asBin(String src) {
		if (src.length() < 1)
			return null;
		byte[] encrypted = new byte[src.length() / 2];
		for (int i = 0; i < src.length() / 2; i++) {
			int high = Integer.parseInt(src.substring(i * 2, i * 2 + 1), 16);//取高位字节
			int low = Integer.parseInt(src.substring(i * 2 + 1, i * 2 + 2), 16);//取低位字节
			encrypted[i] = (byte) (high * 16 + low);
		}
		return encrypted;
	}
}
