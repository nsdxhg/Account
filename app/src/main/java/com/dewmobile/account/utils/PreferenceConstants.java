package com.dewmobile.account.utils;

/**
 * Created by guoqi on 16/4/18.
 */
public class PreferenceConstants {
    public static final String SP_ACCESSIBILITY_FLAG = "sp_accessibility_flag";
    public static final String SP_IMEI = "sp_imei";
    public static final String SP_IMSI = "sp_imsi";
    public static final String SP_ANDROID_ID = "sp_android_id";
    public static final String SP_LOCAL_KEY = "sp_local_key";
}
