package com.dewmobile.account.utils;

/**
 * Created by guoqi on 16/4/7.
 */
public class Constants {
    public static final String HOST = "accountmanage.applinzi.com";
    public static final String LETV_PACKAGE_NAME = "com.letv.android.client";
    public static final String IQIYI_PACKAGE_NAME = "com.qiyi.video";
    public static final String YOUKU_PACKAGE_NAME = "com.youku.phone";

    public static final String ENCODE_KEY_BASE = "dj$hk@ja$d,h;f3*";

    public static final boolean DEBUG_MODEL = true;
    public interface API {
        String GET_LIST_URL = "/getAccountType";
        String GET_ACCOUNT_URL = "/getAccount/?accountType=%s";
    }

    public static String getUrl(String url, Object... args) {
        return "http://" + HOST + String.format(url, args);
    }

    public static String getUrl(String url) {
        return "http://" + HOST + url;
    }
}
