package com.dewmobile.account.utils;

/**
 * Created by guoqi on 16/4/21.
 */
public class EventConst {
    public static final String EVENT_1_1 = "event_1";
    public static final String EVENT_1_2 = "event_2";
    public static final String EVENT_1_3 = "event_3";
    public static final String EVENT_1_4 = "event_4";
    public static final String EVENT_1_5 = "event_5";
    public static final String EVENT_1_6 = "event_6";
}
