package com.dewmobile.account.utils;

import android.content.Context;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by guoqi on 16/4/13.
 */
public class HttpHelper {
    public static void getAccountList(final Context context, Response.Listener<JSONObject> success, Response.ErrorListener error){
        String url = Constants.getUrl(Constants.API.GET_LIST_URL);
        JsonObjectRequest request = new JsonObjectRequest(url,null,success,error){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = NetworkUtil.getHeaders(context);
                return headers;
            }
        };
        VolleyUtils.getDefaultRequestQueue(context).add(request);
    }

    public static void getAccount(final Context context, String category, Response.Listener<JSONObject> success, Response.ErrorListener error){
        String url = Constants.getUrl(Constants.API.GET_ACCOUNT_URL,category);
        JsonObjectRequest request = new JsonObjectRequest(url,null,success,error){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = NetworkUtil.getHeaders(context);
                return headers;
            }
        };
        VolleyUtils.getDefaultRequestQueue(context).add(request);
    }
}
