package com.dewmobile.account.service;

import android.accessibilityservice.AccessibilityService;
import android.accessibilityservice.AccessibilityServiceInfo;
import android.content.BroadcastReceiver;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Message;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;

import com.dewmobile.account.manager.AccountManager;
import com.dewmobile.account.model.AccountModel;
import com.dewmobile.account.processor.LetvProcessor;
import com.dewmobile.account.processor.LoginManager;
import com.dewmobile.account.processor.QiyiProcessor;
import com.dewmobile.account.processor.YoukuProcessor;
import com.dewmobile.account.utils.AccessibilityUtil;
import com.dewmobile.account.utils.Constants;
import com.dewmobile.account.utils.EventConst;
import com.umeng.analytics.MobclickAgent;

import java.util.List;

/**
 * Created by guoqi on 16/3/1.
 */
public class AutoLoginService extends AccessibilityService {
    private AccountModel dataModel = null;
    private LoginManager manager;
    private LocalBroadcastManager broadcastManager;


    @Override
    protected void onServiceConnected() {
        super.onServiceConnected();
        Log.i("gq", "onServiceConnected");
        MobclickAgent.onEvent(this, EventConst.EVENT_1_1);
        AccessibilityServiceInfo info = new AccessibilityServiceInfo();
        info.packageNames = new String[]{Constants.IQIYI_PACKAGE_NAME,
                Constants.LETV_PACKAGE_NAME,
                Constants.YOUKU_PACKAGE_NAME};
        info.eventTypes = AccessibilityEvent.TYPES_ALL_MASK;
        info.feedbackType = AccessibilityServiceInfo.FEEDBACK_ALL_MASK;
        info.notificationTimeout = 100;
        setServiceInfo(info);
        initData();
    }

    private void initData() {
        manager = LoginManager.getInstance();
        broadcastManager = LocalBroadcastManager.getInstance(this);
        IntentFilter filter = new IntentFilter();
        filter.addAction(AccessibilityUtil.ACTION_SEND_DATA);
        broadcastManager.registerReceiver(receiver, filter);
    }

    @Override
    public void onAccessibilityEvent(AccessibilityEvent event) {
        Log.i("gq", event.getEventType()+"----->"+event.getClassName().toString());
        manager.startAutoInput(event, getRootInActiveWindow(), dataModel);
    }

    @Override
    public void onInterrupt() {
        manager = null;
        dataModel = null;
        broadcastManager.unregisterReceiver(receiver);
        broadcastManager = null;
    }

    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            dataModel = (AccountModel) intent.getSerializableExtra("data");
        }
    };
}
