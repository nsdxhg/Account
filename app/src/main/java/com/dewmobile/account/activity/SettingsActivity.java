package com.dewmobile.account.activity;


import android.annotation.TargetApi;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.preference.PreferenceScreen;
import android.preference.SwitchPreference;
import android.provider.Settings;
import android.support.v7.app.ActionBar;
import android.text.TextUtils;
import android.view.MenuItem;
import android.widget.Toast;

import com.dewmobile.account.R;
import com.dewmobile.account.app.MyApplication;
import com.dewmobile.account.manager.DialogManager;
import com.dewmobile.account.utils.AccessibilityUtil;
import com.dewmobile.account.utils.PreferenceConstants;
import com.dewmobile.account.utils.SpUtil;

public class SettingsActivity extends AppCompatPreferenceActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupActionBar();
        setContentView(R.layout.activity_setting);

    }

    private void setupActionBar() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public static class GeneralPreferenceFragment extends PreferenceFragment {
        private DialogManager dialogManager;
        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.pref_general);
            setHasOptionsMenu(true);
            initPreference();
            dialogManager = new DialogManager(getActivity(),DialogManager.DIALOG_TYPE_INPUT_LOCAL_PASS);
        }

        @Override
        public boolean onOptionsItemSelected(MenuItem item) {
            int id = item.getItemId();
            if (id == android.R.id.home) {
                getActivity().onBackPressed();
                return true;
            }
            return super.onOptionsItemSelected(item);
        }

        private void initPreference() {
            Preference editPreference = findPreference("edit_localPass");
            SwitchPreference preferenceAccessibility = (SwitchPreference) findPreference("accessibility");
            bindPreferenceSummaryToValue(preferenceAccessibility);
            editPreference.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(Preference preference) {
                    if ("edit_localPass".equals(preference.getKey())){
                        String localPass = SpUtil.getFromLocal(MyApplication.getContext(), null, PreferenceConstants.SP_LOCAL_KEY, "");
                        if (!TextUtils.isEmpty(localPass)) {
                            dialogManager.getInputLocalPass(new DialogManager.onCheckLocalPass() {
                                @Override
                                public void onMatched(boolean isMatch) {
                                    if (isMatch) {
                                        dialogManager.getSetLocalPass().show();
                                    }else {
                                        Toast.makeText(getActivity(),"密码错误",Toast.LENGTH_SHORT).show();
                                    }
                                }
                            }).show();
                        }else {
                            dialogManager.getSetLocalPass().show();
                        }
                    }
                    return false;
                }
            });
            preferenceAccessibility.setChecked(AccessibilityUtil.isAccessibilityEnabled(getActivity()));
        }

        private void bindPreferenceSummaryToValue(Preference preference) {
            preference.setOnPreferenceChangeListener(sBindPreferenceSummaryToValueListener);
        }

        private Preference.OnPreferenceChangeListener sBindPreferenceSummaryToValueListener = new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object value) {
                if (preference instanceof SwitchPreference) {
                    SwitchPreference switchPreference = (SwitchPreference) preference;
                    if ("accessibility".equals(switchPreference.getKey()) && (boolean) value) {
                        if (!AccessibilityUtil.isAccessibilityEnabled(getActivity())) {
                            startActivity(new Intent(Settings.ACTION_ACCESSIBILITY_SETTINGS));
                        }
                    }
                }
                return true;
            }
        };
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
