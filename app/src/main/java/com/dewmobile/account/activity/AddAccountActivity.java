package com.dewmobile.account.activity;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatSpinner;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.dewmobile.account.R;
import com.dewmobile.account.manager.AccountManager;
import com.dewmobile.account.model.AccountCategory;
import com.dewmobile.account.model.AccountModel;
import com.dewmobile.account.utils.EventConst;
import com.dewmobile.account.utils.PreferenceConstants;
import com.dewmobile.account.utils.SpUtil;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.umeng.analytics.MobclickAgent;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class AddAccountActivity extends BaseActivity {
    private AppCompatSpinner spCategory;
    private AppCompatButton btnCommit;
    private AppCompatEditText etUsername, etPassword;
    private AppCompatCheckBox cbLock;
    private AccountManager accountManager;
    private AccountModel dataModel;
    private boolean isEditModel = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account_add);
        initView();
        initData();
    }

    private void initView() {
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        spCategory = (AppCompatSpinner) findViewById(R.id.sp_category);
        etUsername = (AppCompatEditText) findViewById(R.id.et_username);
        etPassword = (AppCompatEditText) findViewById(R.id.et_password);
        cbLock = (AppCompatCheckBox) findViewById(R.id.cb_lock);
        btnCommit = (AppCompatButton) findViewById(R.id.btn_commit);
        btnCommit.setOnClickListener(mListener);
    }

    private void initData() {
        accountManager = AccountManager.getInstance();
        List<AccountCategory> data_list = AccountCategory.getLocalList(this);
        ArrayAdapter arr_adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, data_list);
        arr_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spCategory.setAdapter(arr_adapter);
        Intent params = getIntent();
        dataModel = (AccountModel) params.getSerializableExtra("data");
        if (dataModel != null) {
            isEditModel = true;
            etUsername.setText(dataModel.username);
            etUsername.setEnabled(false);
            etPassword.setText(dataModel.password);
            cbLock.setChecked(dataModel.type != 0);
            spCategory.setSelection(dataModel.category - 1);
            spCategory.setEnabled(false);
        }

    }

    private void saveAccount() {
        String username = etUsername.getText().toString();
        String password = etPassword.getText().toString();
        if (TextUtils.isEmpty(username) || TextUtils.isEmpty(password)) {
            Toast.makeText(AddAccountActivity.this, "内容不能为空", Toast.LENGTH_SHORT).show();
            return;
        }
        String key = SpUtil.getFromLocal(this, null, PreferenceConstants.SP_LOCAL_KEY, "");
        if (TextUtils.isEmpty(key)) {

        }
        AccountModel model = new AccountModel();
        model.username = username;
        model.password = password;
        model.category = ((AccountCategory) spCategory.getSelectedItem())._id;
        model.type = cbLock.isChecked() ? 1 : 0;
        if (dataModel != null) {
            model._id = dataModel._id;
        }
        new AsyncTask<AccountModel, Void, Void>() {
            @Override
            protected Void doInBackground(AccountModel... params) {
                accountManager.saveAccount(params[0]);
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                MobclickAgent.onEvent(AddAccountActivity.this, EventConst.EVENT_1_3);
                Toast.makeText(AddAccountActivity.this, "已保存", Toast.LENGTH_SHORT).show();
                finish();
            }
        }.execute(model);

    }

    private View.OnClickListener mListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.btn_commit:
                    saveAccount();
                    break;
            }
        }
    };

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
//        getMenuInflater().inflate(R.menu.edit_account, menu);
//        MenuItem menuItem = menu.findItem(R.id.menu_ok);
//        MenuItemCompat.setShowAsAction(menuItem,
//                MenuItemCompat.SHOW_AS_ACTION_ALWAYS);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return false;
    }
}
