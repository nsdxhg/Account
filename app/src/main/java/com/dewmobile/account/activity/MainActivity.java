package com.dewmobile.account.activity;

import android.annotation.TargetApi;
import android.app.AppOpsManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.dewmobile.account.R;
import com.dewmobile.account.fragment.LocalFragment;
import com.dewmobile.account.fragment.OnlineFragment;
import com.dewmobile.account.service.AcountService;
import com.umeng.message.UmengRegistrar;

public class MainActivity extends BaseActivity {
    private FragmentManager fm;
    private LocalFragment localFragment;
    private OnlineFragment onlineFragment;
    private View tabLocal,tabOnline;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(Build.VERSION.SDK_INT>=21){
            getSupportActionBar().setElevation(0);
        }
        setContentView(R.layout.activity_main);
        initControls();
        startService(new Intent(MainActivity.this, AcountService.class));
        if (isStartAccessPermissionSet(MainActivity.this)) {
            startActivity(new Intent(Settings.ACTION_USAGE_ACCESS_SETTINGS));
        }
    }

    private void initControls() {
        String[] titles = new String[]{"本地", "在线"};
        fm = getSupportFragmentManager();
        tabLocal = findViewById(R.id.tab_local);
        tabLocal.setOnClickListener(listener);
        tabOnline = findViewById(R.id.tab_online);
        tabOnline.setOnClickListener(listener);
        setDefaultTab();
        String device_token = UmengRegistrar.getRegistrationId(this);
        Log.i("gq","token-"+device_token);
    }

    private void setDefaultTab(){
        FragmentTransaction ft = fm.beginTransaction();
        if (localFragment == null){
            localFragment = new LocalFragment();
            ft.add(R.id.content, localFragment,"local");
        }
        if (onlineFragment != null){
            ft.hide(onlineFragment);
        }
        ft.show(localFragment);
        ft.commitAllowingStateLoss();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private View.OnClickListener listener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (v == tabLocal){
                tabOnline.findViewById(R.id.iv_icon_cloud).setSelected(false);
                tabLocal.findViewById(R.id.iv_icon_local).setSelected(true);
                FragmentTransaction ft = fm.beginTransaction();
                if (localFragment == null){
                    localFragment = new LocalFragment();
                    ft.add(R.id.content, localFragment,"local");
                }
                if (onlineFragment != null){
                    ft.hide(onlineFragment);
                }
                ft.show(localFragment);
                ft.commitAllowingStateLoss();
            }else if (v == tabOnline){
                tabOnline.findViewById(R.id.iv_icon_cloud).setSelected(true);
                tabLocal.findViewById(R.id.iv_icon_local).setSelected(false);
                FragmentTransaction ft = fm.beginTransaction();
                if (onlineFragment == null){
                    onlineFragment = new OnlineFragment();
                    ft.add(R.id.content, onlineFragment,"online");
                }
                if (localFragment != null){
                    ft.hide(localFragment);
                }
                ft.show(onlineFragment);
                ft.commitAllowingStateLoss();
            }
        }
    };

    @Override
    protected void onDestroy() {
        super.onDestroy();
        stopService(new Intent(MainActivity.this,AcountService.class));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.setting) {
            startActivity(new Intent(this, SettingsActivity.class));
        }
        return false;
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private boolean isStartAccessPermissionSet(Context context){
        try {
            PackageManager pm = context.getPackageManager();
            ApplicationInfo info = pm.getApplicationInfo(context.getPackageName(),0);
            AppOpsManager aom = (AppOpsManager) context.getSystemService(Context.APP_OPS_SERVICE);
            int i = aom.checkOpNoThrow(AppOpsManager.OPSTR_GET_USAGE_STATS,info.uid,info.packageName);
            return i == AppOpsManager.MODE_ALLOWED;
        }catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return false;
    }
}
