package com.dewmobile.account.fragment;

import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.text.InputType;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.dewmobile.account.R;
import com.dewmobile.account.activity.AddAccountActivity;
import com.dewmobile.account.adapter.AccountAdapter;
import com.dewmobile.account.adapter.OnItemClickListener;
import com.dewmobile.account.app.MyApplication;
import com.dewmobile.account.manager.AccountManager;
import com.dewmobile.account.manager.DialogManager;
import com.dewmobile.account.model.AccountCategory;
import com.dewmobile.account.model.AccountModel;
import com.dewmobile.account.utils.AccessibilityUtil;
import com.dewmobile.account.utils.Constants;
import com.dewmobile.account.utils.EventConst;
import com.dewmobile.account.utils.HttpHelper;
import com.dewmobile.account.utils.PackageUtil;
import com.dewmobile.account.utils.PreferenceConstants;
import com.dewmobile.account.utils.SpUtil;
import com.google.gson.Gson;
import com.umeng.analytics.MobclickAgent;

import org.json.JSONObject;

import java.util.List;

public class LocalFragment extends Fragment {
    private RecyclerView rvListView;
    private FloatingActionButton fabAdd;
    private LinearLayoutManager mLayoutManager;
    private AccountAdapter adapter;
    private AccountManager accountManager;
    private LocalBroadcastManager broadcastManager;
    private List<AccountCategory> categoryList;
    private Handler mHandler = new Handler();
    private DialogManager dialogManager;
    private PackageManager packageManager;
    private View emptyView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_local, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        rvListView = (RecyclerView) view.findViewById(R.id.rv_list);
        fabAdd = (FloatingActionButton) view.findViewById(R.id.fab_add);
        emptyView = view.findViewById(R.id.tv_empty);
        initData();
    }

    private void initData() {
        broadcastManager = LocalBroadcastManager.getInstance(getContext());
        accountManager = AccountManager.getInstance();
        packageManager = getActivity().getPackageManager();
        dialogManager = new DialogManager(getActivity(), DialogManager.DIALOG_TYPE_MENU);
        mLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        rvListView.setLayoutManager(mLayoutManager);
        adapter = new AccountAdapter(getActivity(), mListener);
        rvListView.setAdapter(adapter);
        fabAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MobclickAgent.onEvent(getContext(), EventConst.EVENT_1_2);
                startActivity(new Intent(getActivity(), AddAccountActivity.class));
            }
        });
        categoryList = AccountCategory.getLocalList(getActivity());
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    private void getData() {
        new AsyncTask<Void, Void, List<AccountModel>>() {
            @Override
            protected List<AccountModel> doInBackground(Void... params) {
                List<AccountModel> list = accountManager.getAccounts();
                return list;
            }

            @Override
            protected void onPostExecute(List<AccountModel> list) {
                super.onPostExecute(list);
                adapter.setData(list);
                if (list.isEmpty()) {
                    emptyView.setVisibility(View.VISIBLE);
                } else {
                    emptyView.setVisibility(View.GONE);
                }
            }
        }.execute();
    }

    @Override
    public void onResume() {
        super.onResume();
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                getData();
            }
        });
    }

    private OnItemClickListener<AccountModel> mListener = new OnItemClickListener<AccountModel>() {
        @Override
        public void onItemClick(int action, final AccountModel dataModel, int dataPosition) {
            MobclickAgent.onEvent(getContext(), EventConst.EVENT_1_4);
            String localPass = SpUtil.getFromLocal(MyApplication.getContext(), null, PreferenceConstants.SP_LOCAL_KEY, "");
            switch (action) {
                case AccountAdapter.ACTION_ITEM_CLICK:
                    loadLocalAccount(dataModel);
                    break;
                case AccountAdapter.ACTION_ITEM_EDIT:
                    if (dataModel.type == 0) {
                        if (TextUtils.isEmpty(localPass)) {
                            startActivity(new Intent(getActivity(), AddAccountActivity.class).putExtra("data", dataModel));
                            Toast.makeText(getActivity(), "您可以到设置里面设置管理密码", Toast.LENGTH_SHORT).show();
                        } else {
                            dialogManager.getInputLocalPass(new DialogManager.onCheckLocalPass() {
                                @Override
                                public void onMatched(boolean isMatch) {
                                    if (isMatch) {
                                        startActivity(new Intent(getActivity(), AddAccountActivity.class).putExtra("data", dataModel));
                                    } else {
                                        Toast.makeText(getActivity(), "密码错误", Toast.LENGTH_SHORT).show();
                                    }
                                }
                            }).show();
                        }
                    } else {
                        startActivity(new Intent(getActivity(), AddAccountActivity.class).putExtra("data", dataModel));
                    }
                    break;
                case AccountAdapter.ACTION_ITEM_DELETE:
                    if (dataModel.type == 0) {
                        if (TextUtils.isEmpty(localPass)) {
                            startActivity(new Intent(getActivity(), AddAccountActivity.class).putExtra("data", dataModel));
                            Toast.makeText(getActivity(), "您可以到设置里面设置管理密码", Toast.LENGTH_SHORT).show();
                        } else {
                            dialogManager.getInputLocalPass(new DialogManager.onCheckLocalPass() {
                                @Override
                                public void onMatched(boolean isMatch) {
                                    if (isMatch) {
                                        deleteLocalAccount(dataModel._id);
                                    } else {
                                        Toast.makeText(getActivity(), "密码错误", Toast.LENGTH_SHORT).show();
                                    }
                                }
                            }).show();
                        }
                    } else {
                        deleteLocalAccount(dataModel._id);
                    }
                    break;
            }
        }

        @Override
        public void onItemLongClick(int action, final AccountModel dataModel, int dataPosition) {
            MobclickAgent.onEvent(getContext(), EventConst.EVENT_1_5);
            if (action == AccountAdapter.ACTION_ITEM_CLICK) {
                final AccountCategory category = findCategoryById(dataModel.category);
                if (category == null) {
                    Toast.makeText(getActivity(), "不能打开该应用", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (PackageUtil.isApkInstalled(getActivity(), category.pkg)) {
                    String[] items = new String[]{"打开该应用"};
                    dialogManager.getMenu(items, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            switch (which) {
                                case 0:
                                    Intent intent = packageManager.getLaunchIntentForPackage(category.pkg);
                                    startActivity(intent);
                                    break;
                            }
                        }
                    }).show();
                } else {
                    Toast.makeText(getActivity(), "未安装", Toast.LENGTH_SHORT).show();
                }
            }
        }
    };

    private void deleteLocalAccount(final int id) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("删除");
        builder.setMessage("确定要删除该记录吗？删除后不可恢复！");
        builder.setPositiveButton("确定", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                accountManager.deleteAccount(id);
                mHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        getData();
                    }
                });
            }
        });
        builder.setNegativeButton("取消", null);
        builder.create().show();
    }

    private void loadLocalAccount(final AccountModel model) {
        AccountCategory category = findCategoryById(model.category);
        if (category == null) {
            showAccountDialog(model.username, model.password);
            return;
        }
        if (!PackageUtil.isApkInstalled(getActivity(), category.pkg)) {
            Toast.makeText(getActivity(), "未安装", Toast.LENGTH_SHORT).show();
            return;
        }
        if (!AccessibilityUtil.checkAndStart(getActivity())) {
            return;
        }
        ComponentName component = null;
        if (category.pkg.equals(Constants.IQIYI_PACKAGE_NAME)) {
            component = new ComponentName("com.qiyi.video", "org.qiyi.android.video.ui.account.PhoneAccountActivity");
        } else if (category.pkg.equals(Constants.LETV_PACKAGE_NAME)) {
            component = new ComponentName("com.letv.android.client", "com.letv.android.client.activity.LetvLoginActivity");
        } else if (category.pkg.equals(Constants.YOUKU_PACKAGE_NAME)) {
            component = new ComponentName("com.youku.phone", "com.youku.ui.activity.LoginRegistCardViewDialogActivity");
        } else {
            showAccountDialog(model.username, model.password);
        }
        if (component != null) {
            Intent dataIntent = new Intent(AccessibilityUtil.ACTION_SEND_DATA);
            dataIntent.putExtra("data", model);
            broadcastManager.sendBroadcast(dataIntent);
            Intent intent = new Intent();
            intent.setComponent(component);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }
    }

    private void showAccountDialog(String username, String password) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.account_info_dialog, null);
        AppCompatTextView textView = (AppCompatTextView) view.findViewById(R.id.tv_username);
        textView.setText(username);
        final AppCompatEditText etPassword = (AppCompatEditText) view.findViewById(R.id.et_password);
        etPassword.setText(password);
        etPassword.setInputType(InputType.TYPE_CLASS_TEXT);
        builder.setView(view);
        builder.setTitle("获得帐号");
        builder.setCancelable(false);
        builder.create().show();
    }

    private AccountCategory findCategoryById(int id) {
        for (AccountCategory category : categoryList) {
            if (category._id == id) {
                return category;
            }
        }
        return null;
    }
}
