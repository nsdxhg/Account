package com.dewmobile.account.fragment;

import android.app.Dialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.dewmobile.account.R;
import com.dewmobile.account.adapter.AccountAdapter;
import com.dewmobile.account.adapter.AccountCategoryAdapter;
import com.dewmobile.account.adapter.OnItemClickListener;
import com.dewmobile.account.manager.DialogManager;
import com.dewmobile.account.model.AccountCategory;
import com.dewmobile.account.model.AccountModel;
import com.dewmobile.account.utils.AccessibilityUtil;
import com.dewmobile.account.utils.Constants;
import com.dewmobile.account.utils.EncDecUtils;
import com.dewmobile.account.utils.EventConst;
import com.dewmobile.account.utils.HttpHelper;
import com.dewmobile.account.utils.PackageUtil;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.umeng.analytics.MobclickAgent;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class OnlineFragment extends Fragment {
    private static final int SPAN_COUNT = 3;

    private RecyclerView rvListView;
    private GridLayoutManager mLayoutManager;
    private AccountCategoryAdapter adapter;
    private LocalBroadcastManager broadcastManager;
    private SwipeRefreshLayout srlRefresh;
    private Set<Integer> categoryList = new HashSet<>();
    private DialogManager dialogManager;
    private PackageManager packageManager;
    private View emptyView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_online, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        rvListView = (RecyclerView) view.findViewById(R.id.rv_list);
        srlRefresh = (SwipeRefreshLayout) view.findViewById(R.id.srl_refresh);
        emptyView = view.findViewById(R.id.tv_empty);
        initData();
    }

    private void initData() {
        broadcastManager = LocalBroadcastManager.getInstance(getContext());
        packageManager = getActivity().getPackageManager();
        dialogManager = new DialogManager(getContext(), DialogManager.DIALOG_TYPE_MENU);
        mLayoutManager = new GridLayoutManager(getContext(), SPAN_COUNT, GridLayoutManager.VERTICAL, false);
        rvListView.setLayoutManager(mLayoutManager);
        adapter = new AccountCategoryAdapter(getActivity(), mListener);
        rvListView.setAdapter(adapter);
        srlRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getData();
            }
        });
        for (AccountCategory category : AccountCategory.getLocalList(getActivity())) {
            categoryList.add(category._id);
        }
        getData();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    private void getData() {
        HttpHelper.getAccountList(getActivity(), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonObject) {
                Log.i("gq", jsonObject.toString());
                srlRefresh.setRefreshing(false);
                if (!jsonObject.optBoolean("success", true)) {
                    Toast.makeText(getActivity(), jsonObject.optString("data"), Toast.LENGTH_SHORT).show();
                    return;
                }
                JSONArray data = jsonObject.optJSONArray("data");
                Gson gson = new Gson();
                List<AccountCategory> list = gson.fromJson(data.toString(), new TypeToken<List<AccountCategory>>() {
                }.getType());
                adapter.setData(list);
                if (list.isEmpty()){
                    emptyView.setVisibility(View.VISIBLE);
                }else {
                    emptyView.setVisibility(View.GONE);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                srlRefresh.setRefreshing(false);
                emptyView.setVisibility(View.VISIBLE);
            }
        });
    }

    private OnItemClickListener<AccountCategory> mListener = new OnItemClickListener<AccountCategory>() {
        @Override
        public void onItemClick(int action, AccountCategory dataModel, int dataPosition) {
            MobclickAgent.onEvent(getContext(), EventConst.EVENT_1_6);
            if (action == AccountAdapter.ACTION_ITEM_CLICK) {
                if (categoryList.contains(dataModel._id)) {
                    if (!PackageUtil.isApkInstalled(getActivity(), dataModel.pkg)) {
                        Toast.makeText(getActivity(), "未安装", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    if (!AccessibilityUtil.checkAndStart(getActivity())) {
                        return;
                    }
                }
                getRemoteAccount(dataModel);
            }
        }

        @Override
        public void onItemLongClick(int action, final AccountCategory dataModel, int dataPosition) {
            if (action == AccountAdapter.ACTION_ITEM_CLICK) {
                if (PackageUtil.isApkInstalled(getActivity(), dataModel.pkg)) {
                    String[] items = new String[]{"打开该应用"};
                    dialogManager.getMenu(items, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            switch (which) {
                                case 0:
                                    Intent intent = packageManager.getLaunchIntentForPackage(dataModel.pkg);
                                    startActivity(intent);
                                    break;
                            }
                        }
                    }).show();
                } else {
                    Toast.makeText(getActivity(), "未安装", Toast.LENGTH_SHORT).show();
                }
            }
        }
    };

    private void getRemoteAccount(final AccountCategory category) {
        HttpHelper.getAccount(getContext(), String.valueOf(category._id), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonObject) {
                if (!jsonObject.optBoolean("success", true)) {
                    Toast.makeText(getActivity(), "当前这类帐号已发放完毕，请注意通知", Toast.LENGTH_SHORT).show();
                    return;
                }
                Gson gson = new Gson();
                AccountModel model = gson.fromJson(jsonObject.optString("data"), AccountModel.class);
                byte[] encContent = EncDecUtils.decBase64(model.password);
                byte[] decContent = EncDecUtils.decAES(encContent, Constants.ENCODE_KEY_BASE.getBytes(),1);
                String str = new String(decContent);
                model.password = str;
                ComponentName component = null;
                if (category.pkg.equals(Constants.IQIYI_PACKAGE_NAME)) {
                    component = new ComponentName("com.qiyi.video", "org.qiyi.android.video.ui.account.PhoneAccountActivity");
                } else if (category.pkg.equals(Constants.LETV_PACKAGE_NAME)) {
                    component = new ComponentName("com.letv.android.client", "com.letv.android.client.activity.LetvLoginActivity");
                } else if (category.pkg.equals(Constants.YOUKU_PACKAGE_NAME)) {
                    component = new ComponentName("com.youku.phone", "com.youku.ui.activity.LoginRegistCardViewDialogActivity");
                } else {
                    showAccountDialog(model.username, model.password);
                }
                if (component != null) {
                    Intent dataIntent = new Intent(AccessibilityUtil.ACTION_SEND_DATA);
                    dataIntent.putExtra("data", model);
                    broadcastManager.sendBroadcast(dataIntent);
                    Intent intent = new Intent();
                    intent.setComponent(component);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {

            }
        });
    }

    private void showAccountDialog(String username, String password) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.account_info_dialog, null);
        AppCompatTextView textView = (AppCompatTextView) view.findViewById(R.id.tv_username);
        textView.setText(username);
        final AppCompatEditText etPassword = (AppCompatEditText) view.findViewById(R.id.et_password);
        etPassword.setText(password);
        builder.setView(view);
        builder.setTitle("获得帐号");
        builder.setCancelable(false);
        builder.setNegativeButton("查看密码", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                try {
                    Field field = Dialog.class.getDeclaredField("mShowing");
                    field.setAccessible(true);
                    field.set(dialog, false);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                etPassword.setInputType(InputType.TYPE_CLASS_TEXT);
            }
        });
        builder.setPositiveButton("知道了", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                try {
                    Field field = Dialog.class.getDeclaredField("mShowing");
                    field.setAccessible(true);
                    field.set(dialog, true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }
}
